#include <iostream>
using namespace std;

int main()
{
        int X, N, a, b, p;
        p=0;
        cin >> X >> N;

        for(int i=0; i<N; i++)
        {
                cin >> a >> b;
                p += (a*b);
        }

        if(p==X) cout << "Yes" << endl;
        else cout << "No" << endl;

        return 0;
}
