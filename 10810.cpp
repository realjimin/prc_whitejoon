#include <iostream>
#include <vector>
using namespace std;

int main(void)
{
        int n, m;
        cin >> n >> m;
        vector<int> A(n,0);

        while(m>0)
        {
                int i, j, k;
                cin >> i >> j >> k;
                while(i<j+1)
                {
                        A[i-1]=k;
                        i++;
                }
                m--;
        }
        for(int p=0; p<n; p++) cout << A[p] << " ";
        cout << "\n";
        return 0;
}
