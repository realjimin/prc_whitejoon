#include <iostream>
#include <vector>
using namespace std;

int main()
{
        int N, M;
        cin >> N >> M;
        vector<int> A(N,0);
        for(int i=0; i<N; i++) A[i]=i+1;

        while(M>0)
        {
                int i, j;
                cin >> i >> j;
                while(j>i)
                {
                        int t;
                        t=A[i-1];
                        A[i-1]=A[j-1];
                        A[j-1]=t;
                        i++;
                        j--;
                }
                M--;
        }
        for(auto iter : A) cout << iter << " ";
        return 0;
}
