#include <iostream>
using namespace std;

int main()
{
        int  a, b, c;
        cin >> a >> b >> c;

        if(a==b && b==c && c==a) cout << (a*1000)+10000 << endl;
        else if(a==b && b!=c) cout << 1000+(100*a) << endl;
        else if(b==c && c!=a) cout << 1000+(100*c) << endl;
        else if(c==a && a!=b) cout << 1000+(100*a) << endl;
        else
        {
                int max;
                max = a;
                if(max<b) max = b;
                if(max<c) max = c;
                cout << max*100 << endl;
        }
}
