#include <iostream>
#include <vector>
using namespace std;

int main()
{
        int N,M;
        cin >> N >> M;
        vector<int> A(N, 0);
        for(int n=1; n<N+1; n++) A[n-1]=n;

        while(M>0)
        {
                int i,j,t;
                cin >> i >> j;
                t=A[i-1];
                A[i-1]=A[j-1];
                A[j-1]=t;
                M--;
        }
        for(auto iter : A) cout << iter << " ";
        return 0;
}
