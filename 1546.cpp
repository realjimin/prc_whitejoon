#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
        double N, total, favg;
        total=0;
        cin >> N;
        vector<double> A(N,0);

        for(int i=0; i<N; i++) cin >> A[i];
        double l = A[0];
        for(int i=1; i<N; i++) l=max(l, A[i]);
        for(int i=0; i<N; i++) total+=(A[i]/l*100);

        favg = total/N;
        cout << favg << endl;
        return 0;
}
