#include <iostream>
#include <vector>
using namespace std;

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(NULL);
        cout.tie(NULL);

        int T;
        cin >> T;
        vector<int> A(T, 0);
        vector<int> B(T, 0);

        for(int i=0; i<T; i++) cin >> A[i] >> B[i];
        for(int i=0; i<T; i++) cout << "Case #"<< i+1 << ": " << A[i] << " + " << B[i] << " = " << A[i]+B[i] << "\n";

        return 0;
}
