#include <iostream>
#include <vector>
using namespace std;

int main(void)
{
        int N, X;
        cin >> N >> X;

        vector<int> A(N, 0);

        for(int i=0; i<N; i++) cin >> A[i];
        for(int i=0; i<N; i++)
        {
                if(X>A[i]) cout << A[i] << " ";
        }
        cout << "\n";
        return 0;
}
