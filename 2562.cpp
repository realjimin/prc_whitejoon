#include <iostream>
#include <vector>
using namespace std;

int main(void)
{
        vector<int> A(9, 0);
        int max, n;
        for(int i=0; i<9; i++) cin >> A[i];
        max=A[0];
        n=0;
        for(int i=1; i<9; i++)
        {
                if(max<A[i])
                {
                        max=A[i];
                        n=i;
                }
        }
        cout << max << "\n" << n+1 << endl;
        return 0;
}
