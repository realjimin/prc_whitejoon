#include <iostream>
#include <vector>
using namespace std;

int main()
{
        ios::sync_with_stdio(false);
        cin.tie(NULL);
        cout.tie(NULL);

        int T, a, b;
        vector<int> A;
        cin >> T;

        for(int i=0; i<T; i++)
        {
                cin >> a >> b;
                A.push_back(a+b);
        }
        for(int i=0; i<T; i++) cout << "Case #"<< i+1 << ": " << A[i] << "\n";

        return 0;
}
